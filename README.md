# OCR animated gif files #

Perl application, which does very simple character recognition. It reads animated gif file (included in the download). Splits the frames. Each frame is converted to text - if pixel is white symbol is '0' if not symbol '1' is used. The result text image is splited into chars and each char is matched against predefined dictionary. The dictionary is also included in the download. Example item from the library is:

```
00000000
00000000
00000010
00000110
01111110
01111110
00001110
00001110
00001110
00001110
00001110
00001110
00001110
00001110
00001110
00001110
11111111
11111111
00000000
```


The script uses:

    perl5
    Imager
    Text::Levenshtein (included in the project)
