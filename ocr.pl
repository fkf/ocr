#!/usr/bin/perl -w

use lib './lib';
use strict;
use OCR::TextImage;
use Text::Levenshtein;

use constant FUZZY_SEARCH_THRESHOLD => 35;
use constant IMAGE_PATH => './images/';
use constant DICTIONARY_PATH => './dictionary/';

my $file = shift;
die("Format is ocr.pl filename\n")
    if (!$file);

# check if file is gif
die("$file file not supported\n")
    if (! ($file =~ m/.gif$/i) );

# read image frames
my @img = Imager->read_multi(file => IMAGE_PATH.$file)
    or die("Cannot read image file: ".Imager->errstr."\n");

# read dictionary files
my @dfiles;
opendir(my $fh, DICTIONARY_PATH) || die("Cannot open directory");
@dfiles= readdir($fh);
closedir($fh);

my %dictionary;
# init dictionary
foreach my $file (@dfiles) {
    next if ($file =~ m/^\./g);
    my ($char) = ($file =~ m/([A-Z0-9])/gi);
    my $char_text = readFile(DICTIONARY_PATH.$file);
    $dictionary{$char_text} = $char;
}

# result word
my @result = ('?', '?', '?', '?');

my $frame = 0;
# for each frame
for my $img (@img) {
    # create text image
    my $txtimg = OCR::TextImage->new($img);

    # skip blank frames
    next if ($txtimg->isBlank());

    # show text image
    #$txtimg->print();

    # separate chars from the image
    $txtimg->splitChars();

    # get splited chars
    my @chars = @{$txtimg->getChars()};

    # skip twisted frames
    next if (scalar(@chars) < 4);

    for(my $i = 0; $i < 4; $i++) {

        # skip discovered chars
        next if ($result[$i] ne '?');

        # get single string from the char arrayref
        my $char_text = OCR::TextImage::toString($chars[$i]);

        # check if char is defined in the dictionary as it is.
        if ($dictionary{$char_text}) {
            print "Char $i matched in dictionary in $frame frame\n";
            $result[$i] = $dictionary{$char_text};
            next; # skip to next char if recognized
        }

        # fuzzy search the char in the dictionary
        foreach my $key (keys %dictionary) {
            # calculate distance between chars
            my $dist = Text::Levenshtein::fastdistance($key, $char_text);

            if ($dist < FUZZY_SEARCH_THRESHOLD) {
                print "Char $i fuzzy found in $frame frame\n";
                $result[$i] = $dictionary{$key};
                last;  # don't compare to remaining items in dictionary
            }
        }
    }
    # skip other frames if word is discovered
    last if (! (join('', @result) =~ m/\?/));

    $frame++;
} #  end for each frame

print "file $file contains ".join('', @result)." \n";

sub readFile {
    my $filePath = shift;
    return `cat $filePath`;
}