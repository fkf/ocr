package OCR::TextImage;

use strict;
use Imager;
use Data::Dumper;

# params
use constant IMG_DATA => 0;
use constant IMG_WIDTH => 1;
use constant IMG_HEIGHT => 2;
use constant TXT_DATA => 3;
use constant IS_BLANK => 4;
use constant CHARS_DATA => 5;

# settings
use constant DEBUG => 1;
use constant CROP_LEFT => 5;
use constant CROP_RIGHT => 3;
use constant CROP_TOP => 3;
use constant CROP_BOTTOM => 3;
use constant NOISE_REDUCTION => 1;

sub new {
    my ($class, $image) = @_;

    my $textImage = bless ([], $class);

    my $cropped = $image->crop(
        left => CROP_LEFT,
        right => $image->getwidth() - CROP_RIGHT,
        top => CROP_TOP,
        bottom => $image->getheight() - CROP_BOTTOM
    );

    $textImage->[IMG_DATA] = $cropped;
    $textImage->[IMG_WIDTH] = $cropped->getwidth();
    $textImage->[IMG_HEIGHT] = $cropped->getheight();
    $textImage->convertToText();

    print "TextImage debug mode enabled \n" if (DEBUG);

    return $textImage;
}


# convertToText converts image to text.
# white pixels will have value 1
# non white pixels will have value 0
# result is two-dimensional array
sub convertToText {
    my $textImage = shift;

    my $white = Imager::Color->new(red=>255, blue=>255, green=>255);
    my $not_blank;
    my $array;
    # read white pixels
    for (my $h = 0; $h < $textImage->[IMG_HEIGHT]; $h++) {
        my @colors = $textImage->[IMG_DATA]->getscanline(y => $h);
        for (my $w = 0; $w < $textImage->[IMG_WIDTH]; $w++) {
            if ($colors[$w]->equals(other => $white, ignore_alpha => 1)) {
                $array->[$h]->[$w]= 1;
                $not_blank++;
            } else {
                $array->[$h]->[$w]= 0;
            }
        }  # end line
    }  # end all rows

    $textImage->[TXT_DATA] = $array;
    $textImage->[IS_BLANK] = !$not_blank;

    print "convertToText returned ".($not_blank ? '' : 'blank ')."image\n"
        if (DEBUG);
}

# toString concatenate a string from two-dimensional array
sub toString {
    my $text = shift;
    my $result = '';

    for (my $h = 0; $h < scalar(@$text); $h++) {
        for (my $w = 0; $w < scalar(@{ $text->[0] }); $w++) {
            $result .= $text->[$h]->[$w];
        }
        $result .= "\n";
    }
    return $result;
}

# save chars into separate text files
sub saveChars {
    my ($textImage, $file) = @_;
    my $pos = 0;
    foreach my $char (@{ $textImage->[CHARS_DATA] }) {
        open(my $fh, ">".$file."_".$pos) or die($!);
        print $fh toString($char);
        close($fh);
        $pos++;
    }
}

# splitChars scans two-dimentional array by columns.
# all vertical lines containing data are grouped into blocks
# each block represents a character from the image.
# result is arrayref containing two-dimensional arrays
# representing each char.
sub splitChars {
    my $textImage = shift;

    return if ($textImage->[IS_BLANK]);

    # chars contains arrayref of the chars from the image.
    # each char is a two-dimensional arrayref similar to TXT_DATA
    my $chars;

    # points to current char
    my $pos = -1;

    # points to vertical position in current char
    my $xpos = 0;

    # flag showing if previous line was blank
    my $previous_line = 0;

    # init vars
    my $array = $textImage->[TXT_DATA];
    my $height = scalar(@$array);
    my $width = scalar(@{ $array->[0] });

    print "Size is rows: $height cols: $width \n" if (DEBUG);

    # scan columns
    for (my $w = 0; $w < $width; $w++) {
        my $line = 0;
        for (my $h = 0; $h < $height; $h++) {
            $line += $array->[$h]->[$w];
        }

        # remove noise pixels
        $line = $line < NOISE_REDUCTION ? 0 : $line;

        print "Column $w has $line pixels \n" if (DEBUG);

        # char start
        if ($line && !$previous_line) {
            $pos++;
            $xpos = 0;
            print "Start char $pos on column $w \n" if (DEBUG);
        }

        # add column to current char if not blank
        if ($line) {
            print "Add column $w to char $pos on position $xpos \n" if (DEBUG);
            for (my $h = 0; $h < $height; $h++) {
                $chars->[$pos]->[$h]->[$xpos] = $array->[$h]->[$w];
            }
            $xpos++;
        }
        $previous_line = $line;
    }

    $textImage->[CHARS_DATA] = $chars;
}

# output text representation of the image
sub print {
    my $textImage = shift;
    return
        if (! @{$textImage->[TXT_DATA]});
    print toString($textImage->[TXT_DATA]);
}

# getters

sub getImageData {
    return $_[0]->[IMG_DATA];
}

sub getWidth {
    return $_[0]->[IMG_WIDTH];
}

sub getHeight {
    return $_[0]->[IMG_WIDTH];
}

sub isBlank {
    return $_[0]->[IS_BLANK];
}

sub getChars {
    return $_[0]->[CHARS_DATA];
}

1;